
using namespace llvm;

namespace {
  std::string genNodeStart(std::string Element)
  {
      std::string Node1 = "<";
      Node1 += Element;
      Node1 += ">";
      return Node1;
  }
  
  std::string genNodeStop(std::string Element)
  {
      std::string Node2 = "</";
      Node2 += Element;
      Node2 += ">";
      return Node2;
  }
  
  std::string genInnerXmlNode(std::string Element, std::string Text)
  {
      std::string Node = genNodeStart(Element);
  //    Node += " \n";
      Node += Text;
  //    Node += " \n";
      Node += genNodeStop(Element);
      return Node;
  }
  
  std::string getGlobalArrXml(std::string funcName, std::string ArrName, std::string ArrType, std::string ArrNumElements){
    std::string FuncNameXmlStr = genInnerXmlNode("Func",funcName);
    std::string ArrNameXmlStr = genInnerXmlNode("ArrName",ArrName);
    std::string ArrTypeXmlStr = genInnerXmlNode("Type",ArrType);
    std::string ArrNumElemXmlStr = genInnerXmlNode("NumElem",ArrNumElements);
    std::string completeXmlNode = genInnerXmlNode("GlobalArr",FuncNameXmlStr + ArrNameXmlStr +  ArrTypeXmlStr +  ArrNumElemXmlStr);
    return completeXmlNode;
  }

  std::string getArgsXml(std::string funcName, std::string ArgName, std::string ArgType){
    std::string FuncNameXmlStr = genInnerXmlNode("Func",funcName);
    std::string ArgNameXmlStr = genInnerXmlNode("ArgName",ArgName);
    std::string ArgTypeXmlStr = genInnerXmlNode("Type",ArgType);
    std::string completeXmlNode = genInnerXmlNode("Arg",FuncNameXmlStr + ArgNameXmlStr + ArgTypeXmlStr );
    return completeXmlNode;
  }
}
