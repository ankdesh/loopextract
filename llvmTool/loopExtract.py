import os
import sys
import subprocess
import xml.dom.minidom as minidom

def check_env():
  pathDirs = os.environ['PATH'].split(":")
  clangAvailable = False
  for dir in pathDirs:
      if os.path.isfile(dir+'/clang'):
        clangAvailable = True
  assert clangAvailable == True, "clang not in path"
  cmd = 'opt -load LLVMLoop2Prog.so -loop2prog -help | grep loop2prog'
  optResult = docmdSerial(cmd)
  assert optResult.count > 0, "Cannot load LLVMLoop2Prog.so or -loop2prog not found"
         

def docmdSerial(cmd = ''):
  assert cmd != '', "No cmd passed"
  listOfOutput = []
  print "Executing... " + cmd 
  p = subprocess.Popen(cmd, shell = True, stdout = subprocess.PIPE, stderr = subprocess.STDOUT)  
  for line in p.stdout.readlines():
      listOfOutput.append(line)
  return listOfOutput

def findFileNames(dir = '.'):
  listFileNamesWoExt = []
  for fileName in os.listdir(dir):
    if fileName.endswith('.c'):
        listFileNamesWoExt.append(fileName.rsplit('.c')[0])
  return listFileNamesWoExt

def emitLLVMclang(listFileNamesWoExt):
  for eachFileName in listFileNamesWoExt:
    cmd = 'clang -c -emit-llvm -o ' + eachFileName + '.bc ' + eachFileName + '.c'
    clangReturn = docmdSerial(cmd)
    print clangReturn

def llvmLoopExtract(listFileNamesWoExt):
  for eachFileName in listFileNamesWoExt:
    cmd = 'opt -loop-extract -o ' + eachFileName + '.loopextract.bc ' + eachFileName + '.bc'
    extractLoopReturn = docmdSerial(cmd)
    print extractLoopReturn

def Allbc2ll(dir = '.'):
  listbcFileNames = []
  for fileName in os.listdir(dir):
    if fileName.endswith('.bc'):
      cmd = 'llvm-dis ' + fileName 
      bc2llReturn = docmdSerial(cmd)
      print bc2llReturn

def findParameters(listFileNamesWoExt):
  for eachFileName in listFileNamesWoExt:
    cmd = 'opt -load LLVMLoop2Prog.so -loop2prog ' + eachFileName + '.loopextract.bc >/dev/null'
    returnXml = docmdSerial(cmd)
    properXml = "<head>" + returnXml[0] + "</head>"
    parsedXml = minidom.parseString(properXml)
    fileXml = open(eachFileName+'.progAttr.xml','w')
    fileXml.write(parsedXml.toprettyxml(indent = "\t"))
    fileXml.close()
    print parsedXml.toxml()


if __name__ == '__main__':
  check_env()
  cfileNames = findFileNames()
  print cfileNames
  emitLLVMclang(cfileNames)
  llvmLoopExtract(cfileNames)
  findParameters(cfileNames)
  Allbc2ll()
