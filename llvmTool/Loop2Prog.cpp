//===- Loop2Prog.cpp - Converts loops into standalone progs ---------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

#include "llvm/Instructions.h"
#include "llvm/Pass.h"
#include "llvm/Function.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/circular_raw_ostream.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/ADT/StringExtras.h"
#include "xmlResults.h"
#include "typeResolve.h"
using namespace llvm;

namespace {
  struct Loop2Prog : public FunctionPass {
    static char ID; // Pass identification, replacement for typeid
    Loop2Prog() : FunctionPass(ID) {}

    virtual bool runOnFunction(Function &F) {
      std::string funcName = F.getName(); 
//    Printing List of Gloabal Vars accessed
      for (Function::iterator BB = F.begin(), E = F.end(); BB != E; ++BB) {
        for (BasicBlock::iterator I = BB->begin(), E = BB->end(); I != E; ++I) {
          if (isa<GetElementPtrInst>(*I))
          {
            GetElementPtrInst *LI = dyn_cast<GetElementPtrInst>(I);
            // errs() << LI->getOpcodeName() <<"  "<< LI->getPointerOperand()->getName() << "\n";
            if (isa<PointerType >(LI->getPointerOperand()->getType())) //isa Pointer Type
            {
             PointerType *PT = dyn_cast<PointerType>(LI->getPointerOperand()->getType()); 
             if(isa<ArrayType>(PT->getElementType())) // PT Points to an Array
             { 
               std::string arrayType = getStringForType(PT->getElementType()->getArrayElementType());
               std::string arrayName = LI->getPointerOperand()->getName() ;
               std::string numArrayElements = utostr_32(PT->getElementType()->getArrayNumElements());
               errs()<< getGlobalArrXml(funcName, arrayName, arrayType, numArrayElements);
             }
            }
          }
        }
      }

      //Printing List of Arguments
      for (Function::arg_iterator I = F.arg_begin(), E = F.arg_end(); I != E; ++I) {
        Argument *Arg = I;
        Type *argType = Arg->getType();
        std::string typeName ="";
        std::string argName = Arg->getName(); 
        if (isa<PointerType>(argType))
        {
          PointerType *PT = dyn_cast<PointerType>(argType);
          argType = PT->getElementType();
          if(argType->isIntegerTy() || argType->isFloatingPointTy()){
            typeName += "ptr2";
          }
          else if(argType->isArrayTy()){
            typeName += "ptr2arrayOf";
            argType = argType->getArrayElementType();
          }
          else{
            errs()<<"Only pointer to Int,float and array are handled currently";
          }
        }
        typeName += getStringForType(argType);
        errs()<< getArgsXml(funcName, argName, typeName);
      }
      return false;
    }

    // We don't modify the program, so we preserve all analyses
    virtual void getAnalysisUsage(AnalysisUsage &AU) const {
      AU.setPreservesAll();
    }
  };
}

char Loop2Prog::ID = 0;
static RegisterPass<Loop2Prog>
Y("loop2prog", "Pass to generate stand alone progs from loops");
