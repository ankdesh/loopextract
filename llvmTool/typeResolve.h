 #include "llvm/Assembly/Writer.h"
 #include "llvm/Assembly/PrintModulePass.h"
 #include "llvm/Assembly/AssemblyAnnotationWriter.h"
 #include "llvm/LLVMContext.h"
 #include "llvm/CallingConv.h"
 #include "llvm/Constants.h"
 #include "llvm/DerivedTypes.h"
 #include "llvm/InlineAsm.h"
 #include "llvm/IntrinsicInst.h"
 #include "llvm/Operator.h"
 #include "llvm/Module.h"
 #include "llvm/ValueSymbolTable.h"
 #include "llvm/ADT/DenseMap.h"
 #include "llvm/ADT/SmallString.h"
 #include "llvm/ADT/StringExtras.h"
 #include "llvm/ADT/STLExtras.h"
 #include "llvm/Support/CFG.h"
 #include "llvm/Support/Debug.h"
 #include "llvm/Support/Dwarf.h"
 #include "llvm/Support/ErrorHandling.h"
 #include "llvm/Support/MathExtras.h"
 #include "llvm/Support/FormattedStream.h"
 #include <algorithm>
 #include <cctype>
 using namespace llvm;
namespace{
 std::string getStringForType(Type *Ty) {
  std::string stringForType;
  unsigned sizeOfInt; 
  switch (Ty->getTypeID()) {
  case Type::VoidTyID:      stringForType = "void"; break;
  case Type::HalfTyID:      stringForType = "half"; break;
  case Type::FloatTyID:     stringForType = "float"; break;
  case Type::DoubleTyID:    stringForType = "double"; break;
  case Type::X86_FP80TyID:  stringForType = "x86_fp80"; break;
  case Type::FP128TyID:     stringForType = "fp128"; break;
  case Type::PPC_FP128TyID: stringForType = "ppc_fp128"; break;
  case Type::LabelTyID:     stringForType = "label"; break;
  case Type::MetadataTyID:  stringForType = "metadata"; break;
  case Type::X86_MMXTyID:   stringForType = "x86_mmx"; break;
  case Type::IntegerTyID:
    sizeOfInt = cast<IntegerType>(Ty)->getBitWidth();
    switch(sizeOfInt){
        case 32: stringForType = "int"; break;
        case 16: stringForType = "short"; break;
        case 8:  stringForType = "char"; break;
        default: stringForType = "<unrecognized-type>";
      }
    break;  
   //Not Handling following types now.
  //case Type::FunctionTyID: {
  //  FunctionType *FTy = cast<FunctionType>(Ty);
  //  print(FTy->getReturnType(), OS);
  //  OS << " (";
  //  for (FunctionType::param_iterator I = FTy->param_begin(),
  //       E = FTy->param_end(); I != E; ++I) {
  //    if (I != FTy->param_begin())
  //      OS << ", ";
  //    print(*I, OS);
  //  }
  //  if (FTy->isVarArg()) {
  //    if (FTy->getNumParams()) OS << ", ";
  //    OS << "...";
  //  }
  //  OS << ')';
  //  return;
  //}
  //case Type::StructTyID: {
  //  StructType *STy = cast<StructType>(Ty);

  //  if (STy->isLiteral())
  //    return printStructBody(STy, OS);

  //  if (!STy->getName().empty())
  //    return PrintLLVMName(OS, STy->getName(), LocalPrefix);

  //  DenseMap<StructType*, unsigned>::iterator I = NumberedTypes.find(STy);
  //  if (I != NumberedTypes.end())
  //    OS << '%' << I->second;
  //  else  // Not enumerated, print the hex address.
  //    OS << "%\"type " << STy << '\"';
  //  return;
  //}
  //case Type::PointerTyID: {
  //  PointerType *PTy = cast<PointerType>(Ty);
  //  print(PTy->getElementType(), OS);
  //  if (unsigned AddressSpace = PTy->getAddressSpace())
  //    OS << " addrspace(" << AddressSpace << ')';
  //  OS << '*';
  //  return;
  //}
  //case Type::ArrayTyID: {
  //  ArrayType *ATy = cast<ArrayType>(Ty);
  //  OS << '[' << ATy->getNumElements() << " x ";
  //  print(ATy->getElementType(), OS);
  //  OS << ']';
  //  return;
  //}
  //case Type::VectorTyID: {
  //  VectorType *PTy = cast<VectorType>(Ty);
  //  OS << "<" << PTy->getNumElements() << " x ";
  //  print(PTy->getElementType(), OS);
  //  OS << '>';
  //  return;
  //}
  default:
    stringForType = "<unrecognized-type>";
  }
    return stringForType;
}
}
