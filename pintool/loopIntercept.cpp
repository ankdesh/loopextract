#include "pin.H"

#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cassert>
#include <cstdlib>
#include <vector>
#include <list>
#include <map>

using namespace std;

#define PROGRAMNAME "fast-edge"

ofstream MemoryOutFile;
ofstream ParameterOutFile;

KNOB<string> KnobMemoryOutputFile(KNOB_MODE_WRITEONCE, "pintool", "m", "memoryvalues.out", "Specify Memory Values output file");
KNOB<string> KnobParametersOutputFile(KNOB_MODE_WRITEONCE, "pintool", "p", "parametervalues.out", "Specify Parameter Values output file");
KNOB<string> KnobFunctionListInputFile(KNOB_MODE_WRITEONCE, "pintool", "f", "functions.in", "Specify Function Names Input file");
KNOB<string> KnobVariableListInputFile(KNOB_MODE_WRITEONCE, "pintool", "v", "variables.in", "Specify Variable Names Input file");

typedef enum {Char, Short, Integer, Long, Float, Double, UnsignedChar,
UnsignedShort, UnsignedInt, UnsignedLong, CharPtr, ShortPtr, IntegerPtr,
LongPtr, FloatPtr, DoublePtr, UnsignedCharPtr, UnsignedShortPtr, 
UnsignedIntPtr, UnsignedLongPtr, CharRef, ShortRef, IntegerRef, LongRef,
FloatRef, DoubleRef, UnsignedCharRef, UnsignedShortRef, UnsignedIntRef, 
UnsignedLongRef, VoidPtr} VariableTypes;

map<string, VariableTypes> StringToType;
map<VariableTypes, string> TypeToString;

UINT32 sizeOfType[] = {1, 2, 4, 8, 4, 8, 1, 2, 4, 8, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 1, 2, 4, 8, 4, 8, 1, 2, 4, 8, 4};

struct FunctionDescription {
	INT32 numberOfArguments;
	vector< pair<VariableTypes, bool> > argumentDescription;
	UINT32 lastParameterReceived;
	FunctionDescription() {
		numberOfArguments = 0;
		lastParameterReceived = -1;
	}
	FunctionDescription(const INT32 numArgs, const vector< pair<VariableTypes, bool> >& argsDesc) {
		numberOfArguments = numArgs;
		argumentDescription = argsDesc;
		lastParameterReceived = -1;
	}
};

map<string, FunctionDescription> functionsOfInterest;

struct VariableDescription {
	string variableName;
	VariableTypes type;
	ADDRINT endAddress;
	bool pointerType;
	VariableDescription() {
		variableName = "";
		type = Char;
		endAddress = 0;
		pointerType = false;
	}
	VariableDescription(const string& name, VariableTypes typ, ADDRINT endAddr, bool ptrType) {
		variableName = name;
		type = typ;
		endAddress = endAddr;
		pointerType = ptrType;
	}
};

list<ADDRINT> addressesOfInterest;
map<ADDRINT, VariableDescription> variablesOfInterest;

struct DataValues {
	bool ptrType;
	VariableTypes typeOfData;
	UINT64 value;
	ADDRINT addr;
	DataValues () {
		ptrType = false;
		typeOfData = Char;
		value = 0;
		addr = -1;
	}
	DataValues (UINT32 inVal, VariableTypes dataType, bool ptr) 
		: ptrType(ptr),typeOfData(dataType), value(inVal) {
		addr = -1;
	}
	DataValues (UINT32 inVal, ADDRINT addr, VariableTypes dataType, bool ptr) 
		: ptrType(ptr),typeOfData(dataType), value(inVal), addr(addr) {
	}
};

map<ADDRINT, DataValues> memoryDump;

map< pair<string, UINT32>, DataValues> parameterValueDump;

void splitStrings(string& input, const string& splitChar, vector<string>& listSubstrings) {
	size_t pos = 0;
	size_t newpos = 0;
	while((newpos = input.find(splitChar, pos)) != string::npos) {
		listSubstrings.push_back(input.substr(pos, (newpos-pos)));
		pos = newpos+1;
	}
	if (newpos == string::npos) {
		listSubstrings.push_back(input.substr(pos, string::npos));
	}
}

void strip(string& input) {
	size_t pos = -1;
	for(UINT32 i = 0; i < input.size(); i++) {
		if (!isspace(input[i])) {
			pos = i;
			break;
		}
	}
	input.erase(0, pos);
	pos = string::npos;
	for (UINT32 i = input.size()-1; i >= 0; i++) {
		if (!isspace(input[i])) {
			pos = i+1;
			break;
		}
	}
	input.erase(pos, string::npos);
}

string getString(const VariableTypes type) {
	if (TypeToString.size() == 0) {
		TypeToString[Char] = string("char");
		TypeToString[Short] = string("short");
		TypeToString[Integer] = string("int");
		TypeToString[Long] = string("long long");
		TypeToString[Float] = string("float");
		TypeToString[Double] = string("double");
		TypeToString[UnsignedChar] = string("unsigned char");
		TypeToString[UnsignedShort] = string("unsigned short");
		TypeToString[UnsignedInt] = string("unsigned int");
		TypeToString[UnsignedLong] = string("unsigned long long");
		TypeToString[CharPtr] = string("char *");
		TypeToString[ShortPtr] = string("short *");
		TypeToString[IntegerPtr] = string("int *");
		TypeToString[LongPtr] = string("long long *");
		TypeToString[FloatPtr] = string("float *");
		TypeToString[DoublePtr] = string("double *");
		TypeToString[UnsignedCharPtr] = string("unsigned char *");
		TypeToString[UnsignedShortPtr] = string("unsigned short *");
		TypeToString[UnsignedIntPtr] = string("unsigned int *");
		TypeToString[UnsignedLongPtr] = string("unsigned long long *");
		TypeToString[CharRef] = string("char &");
		TypeToString[ShortRef] = string("short &");
		TypeToString[IntegerRef] = string("int &");
		TypeToString[LongRef] = string("long long &");
		TypeToString[FloatRef] = string("float &");
		TypeToString[DoubleRef] = string("double &");
		TypeToString[UnsignedCharRef] = string("unsigned char &");
		TypeToString[UnsignedShortRef] = string("unsigned short &");
		TypeToString[UnsignedIntRef] = string("unsigned int &");
		TypeToString[UnsignedLongRef] = string("unsigned long &");
		TypeToString[VoidPtr] = string("void *");
	}
	return TypeToString[type];
}

VariableTypes determineType(const string& type) {
	if (StringToType.size() == 0) {
		StringToType[string("char")] = Char;
		StringToType[string("short")] = Short;
		StringToType[string("int")] = Integer;
		StringToType[string("long long")] = Long;
		StringToType[string("float")] = Float;
		StringToType[string("double")] = Double;
		StringToType[string("unsigned char")] = UnsignedChar;
		StringToType[string("unsigned short")] = UnsignedShort;
		StringToType[string("unsigned int")] = UnsignedInt;
		StringToType[string("unsigned long long")] = UnsignedLong;
		StringToType[string("char *")] = CharPtr;
		StringToType[string("short *")] = ShortPtr;
		StringToType[string("int *")] = IntegerPtr;
		StringToType[string("long long *")] = LongPtr;
		StringToType[string("float *")] = FloatPtr;
		StringToType[string("double *")] = DoublePtr;
		StringToType[string("unsigned char *")] = UnsignedCharPtr;
		StringToType[string("unsigned short *")] = UnsignedShortPtr;
		StringToType[string("unsigned int *")] = UnsignedIntPtr;
		StringToType[string("unsigned long long *")] = UnsignedLongPtr;
		StringToType[string("char &")] = CharRef;
		StringToType[string("short &")] = ShortRef;
		StringToType[string("int &")] = IntegerRef;
		StringToType[string("long long &")] = LongRef;
		StringToType[string("float &")] = FloatRef;
		StringToType[string("double &")] = DoubleRef;
		StringToType[string("unsigned char &")] = UnsignedCharRef;
		StringToType[string("unsigned short &")] = UnsignedShortRef;
		StringToType[string("unsigned int &")] = UnsignedIntRef;
		StringToType[string("unsigned long &")] = UnsignedLongRef;
		StringToType[string("void *")] = VoidPtr;
	}
	return StringToType[type];
}

IARG_TYPE determinePinType(const VariableTypes type) {
	switch (type) {
		case Char:
		case Short:
		case Integer:
		case Float:
		case UnsignedChar:
		case UnsignedShort:
		case UnsignedInt:
		case CharRef:
		case IntegerRef:
		case ShortRef:
		case FloatRef:
			return IARG_UINT32;
			break;
		case CharPtr:
		case ShortPtr:
		case IntegerPtr:
		case LongPtr:
		case FloatPtr:
		case DoublePtr:
		case UnsignedCharPtr:
		case UnsignedShortPtr:
		case UnsignedIntPtr:
		case UnsignedLongPtr:
		case VoidPtr:
			return IARG_PTR;
			break;
		default:
			assert(false);
	}
	return IARG_PTR;
}

bool isPtrType(const VariableTypes type) {
	switch (type) {
		case CharPtr:
		case ShortPtr:
		case IntegerPtr:
		case LongPtr:
		case FloatPtr:
		case DoublePtr:
		case UnsignedCharPtr:
		case UnsignedShortPtr:
		case UnsignedIntPtr:
		case UnsignedLongPtr:
		case VoidPtr:
			return true;
			break;
		default:
			return false;
	}
	return false;
}

VOID ParseFunctionInputFile() {
	//Function input file has the following structure:
	//function name, num of arguments, argument type1, .. \n terminates it
	ifstream FunctionsInFile;
	FunctionsInFile.open(KnobFunctionListInputFile.Value().c_str());
	string line;
	while(!FunctionsInFile.eof()) {
		getline(FunctionsInFile, line);
		vector<string> listSubstrings;
		splitStrings(line, string(","), listSubstrings);
		if (line.size() == 0)
			continue;
		assert(listSubstrings.size() >= 2);
		string functionName = listSubstrings[0]; 
		strip(functionName);
		string numArgumentsString = listSubstrings[1];
		istringstream sstr(numArgumentsString.c_str());
		int numArgs = 0;
		sstr >> numArgs;
		vector< pair<VariableTypes, bool> > argsDesc;
		for (int i = 0; i < numArgs; i++) {
			VariableTypes type = determineType(listSubstrings[i+2]);
			bool isPtr = isPtrType(type);
			argsDesc.push_back(pair<VariableTypes, bool>(type, isPtr));
		}
		FunctionDescription funcDesc(numArgs, argsDesc);
		functionsOfInterest[functionName] = funcDesc;
		cout << functionName << " " << numArgs << " " << argsDesc.size() << endl;
	}
	cout << "Function Parsing completed" << endl;
	FunctionsInFile.close();
}

VOID ParseVariableInputFile(ADDRINT startAddress) {
	//The format for variables: name, address, type, range
	string line;
	ifstream VariablesInFile;
	VariablesInFile.open(KnobVariableListInputFile.Value().c_str());
	if (VariablesInFile.bad()) {
		cout << "File open failed. " << KnobVariableListInputFile.Value().c_str() << endl;
		exit(-1);
	}
	while(!VariablesInFile.eof()) {
		getline(VariablesInFile, line);
		if (line.size() == 0)
			continue;
		vector<string> listSubstrings;
		splitStrings(line, string(","), listSubstrings);
		assert(listSubstrings.size() == 4);
		string variableName = listSubstrings[0];
		strip(variableName);
		string variableAddress = listSubstrings[1];
		strip(variableAddress);
		istringstream sstr;
		sstr.str(variableAddress);
		ADDRINT varAddr = 0;
		sstr >> hex >> varAddr;
		varAddr += startAddress;
		string dataTypeStr = listSubstrings[2];
		strip(dataTypeStr);
		VariableTypes dataType = determineType(dataTypeStr);
		bool ptrType = isPtrType(dataType);
		string variableRange = listSubstrings[3];
		strip(variableRange);
		sstr.str(variableRange);
		UINT32 varRange = 0;
		sstr >> varRange;
		ADDRINT endAddr = varAddr + varRange*sizeOfType[dataType];
		VariableDescription varDesc(variableName, dataType, endAddr, ptrType);
		list<ADDRINT>::iterator i = addressesOfInterest.begin();
		for (;i != addressesOfInterest.end(); ++i) {
			if ((*i) > varAddr)
				break;
		}
		addressesOfInterest.insert(i, varAddr);
		variablesOfInterest[varAddr] = varDesc;
		cout << variableName << " " << varAddr << " " << endAddr << " " << ptrType << " " << dataType << endl;
	}
	cout << "Variable Parsing completed " << endl;
	VariablesInFile.close();
}

UINT32 convertFloatToInt(float f) {
	union {
		float f;
		UINT32 i;
	} u;
	u.f = f;
	return u.i;
}

UINT32 accessValue(ADDRINT addr, VariableTypes dataType) {
	UINT32 val;
	switch (dataType) {
		case CharPtr:{
			char s_val = *(char *)addr;
			val = (UINT32)(s_val);
			break;
		}
		case ShortPtr:{
			short s_val = *(short *)addr;
			val = (UINT32)(s_val);
			break;
		}
		case IntegerPtr:{
			int s_val = *(int *)addr;
			val = (UINT32)(s_val);
			break;
		}
		case LongPtr:
			assert(false);
			break;
		case FloatPtr: {
			float s_val = *(float *)addr;
			val = convertFloatToInt(s_val);
			break;
		}
		case DoublePtr:
			assert(false);
			break;
		case UnsignedCharPtr: {
			char s_val = *(unsigned char *)addr;
			val = (UINT32)s_val;
			break;
		}
		case UnsignedShortPtr: {
			short s_val = *(unsigned short *)addr;
			val = (UINT32)s_val;
			break;
		}
		case UnsignedIntPtr: {
			val = *(UINT32 *)addr;
			break;
		}
		case UnsignedLongPtr:
			assert(false);
			break;
		case VoidPtr:
			assert(false);
			break;
		default:
			assert(false);
			break;
	}
	return val;
}

VOID ArgAddrRecorder(CHAR *name, ADDRINT addr) {
	FunctionDescription f = functionsOfInterest[string(name)];
	UINT32 argVal = f.lastParameterReceived + 1;
	pair<VariableTypes, bool> dataType = f.argumentDescription[argVal];
	UINT32 val = accessValue(addr, dataType.first);
	cout << "In ArgAddrRecorder: " << name << " " << val << endl;
	DataValues d(val, addr, dataType.first, dataType.second);
	parameterValueDump[pair<string, UINT32>(string(name), argVal)] = d;
}

VOID ArgValRecorder(CHAR * name, UINT32 val) {
	cout << "In ArgValRecorder: " << name << " " << val << endl;
	FunctionDescription f = functionsOfInterest[string(name)];
	UINT32 argVal = f.lastParameterReceived + 1;
	pair<VariableTypes, bool> dataType = f.argumentDescription[argVal];
	DataValues d(val, dataType.first, dataType.second);
	parameterValueDump[pair<string, UINT32>(string(name), argVal)] = d;
}

VOID RecordMemRead(VOID * ip, VOID * addr) {
	UINT32 addrVal = (UINT32)addr;
	list<ADDRINT>::iterator prev_i = addressesOfInterest.begin();
	for (list<ADDRINT>::iterator i = addressesOfInterest.begin(); 
		i != addressesOfInterest.end(); ++i) {
		if ((*i) >= addrVal)
			break;
		else
			prev_i = i;	
	}
	if (prev_i != addressesOfInterest.end()) {
		VariableDescription varDesc = variablesOfInterest[(*prev_i)];
		if (varDesc.endAddress > addrVal) {
			UINT32 val = accessValue(addrVal, varDesc.type);
			DataValues d(val, addrVal, varDesc.type, varDesc.pointerType);
			memoryDump[addrVal] = d;
		}
	}
}

VOID ImageLoad(IMG img, VOID *v) {
	//Parse the input files and populate the datastructures
	if(IMG_Name(img).find(PROGRAMNAME) == string::npos) {
		cout << "Image Name does not match: " << IMG_Name(img).c_str() << " != " << PROGRAMNAME << endl;
		return;
	}
	ADDRINT startAddress = IMG_LoadOffset(img);
	cout << "Address Offset = " << hex << startAddress << dec << " " << IMG_Name(img).c_str() << endl;
	ParseVariableInputFile(startAddress);
	ParseFunctionInputFile();

	//For each function name in functionsOfInterest
	cout << functionsOfInterest.size() << endl;
	for (map<string, FunctionDescription>::iterator i = functionsOfInterest.begin();
		i != functionsOfInterest.end(); i++) {
		/* Part 1: getting input values of parameters */
		// get rtn from name
		RTN funcRtn = RTN_FindByName(img, (*i).first.c_str());
		if (!RTN_Valid(funcRtn)) {
			cout << "Unable to find routine: " << (*i).first.c_str() << endl;
			continue;
		}
		cout << "Found Routine: " << (*i).first.c_str() << endl;
		RTN_Open(funcRtn);
		// for every parameter in the argumentDescription 
		for (int j = 0; j < (*i).second.numberOfArguments; ++j) {
			IARG_TYPE argDataType = determinePinType((*i).second.argumentDescription[j].first);
			// add callback for the argument. 
			if (!(*i).second.argumentDescription[j].second) {
				RTN_InsertCall(funcRtn, IPOINT_BEFORE, (AFUNPTR)ArgValRecorder,
								argDataType, (*i).first.c_str(), 
								IARG_FUNCARG_ENTRYPOINT_VALUE, j,
								IARG_END);
				cout << "Added Val interception " << j << endl;
			}
			else {
				RTN_InsertCall(funcRtn, IPOINT_BEFORE, (AFUNPTR)ArgAddrRecorder,
								argDataType, (*i).first.c_str(), 
								IARG_FUNCARG_ENTRYPOINT_VALUE, j,
								IARG_END);
				cout << "Added Addr interception " << j << endl;
			}
		}
	
		/* Part 2: Instruction Instrumentation */
		//Visit every instruction of the function
		for (INS ins = RTN_InsHead(funcRtn); INS_Valid(ins); ins = INS_Next(ins)) {
			//get INS_MemoryOperandCount for this instruction
			UINT32 memOperands = INS_MemoryOperandCount(ins);
			//for every memory operand of that instruction
			for (UINT32 memOp = 0; memOp < memOperands; memOp++) {
				//if INS_MemoryOperandIsRead
				if (INS_MemoryOperandIsRead(ins, memOp)) {
					//then register load value callback. This function 
					//records data for first access to the address
					INS_InsertPredicatedCall(ins, IPOINT_BEFORE, (AFUNPTR)RecordMemRead,
								IARG_INST_PTR, IARG_MEMORYOP_EA, memOp, IARG_END);
				}
			}
		}
		RTN_Close(funcRtn);
	}

}

VOID DumpSavedValues(INT32 code, VOID *v) {
	//Dump saved values here
	for(map<UINT32, DataValues>::iterator i = memoryDump.begin(); i != memoryDump.end(); i++) {
		MemoryOutFile << (*i).first << "," << (*i).second.typeOfData << "," 
					<< hex << (*i).second.addr << "," << (*i).second.ptrType
					<< (*i).second.value << dec << endl;
	}
	MemoryOutFile.close();
	for (map< pair<string, UINT32>, DataValues>::iterator i = parameterValueDump.begin();
		i != parameterValueDump.end(); ++i) {
		ParameterOutFile << (*i).first.first << "," << (*i).first.second
					<< hex << (*i).second.addr << "," << (*i).second.ptrType
					<< (*i).second.value << dec << endl;
	}
	ParameterOutFile.close();
}

INT32 Usage() {
	cerr << "This tool determines the input parameter values to a set of "
		 << "specified functions and the values of global variables at the "
		 << "start of execution" << endl;
	cerr << endl << KNOB_BASE::StringKnobSummary() << endl;
	return -1;
}

int main(int argc, char* argv[]) {
	PIN_InitSymbols();
	if (PIN_Init(argc, argv)) {
		return Usage();
	}

	cout << "Pin Init completed " << endl;
	MemoryOutFile.open(KnobMemoryOutputFile.Value().c_str());
	ParameterOutFile.open(KnobParametersOutputFile.Value().c_str());

	cout << "Adding Instrument Function " << endl;
	IMG_AddInstrumentFunction(ImageLoad, 0);	
	PIN_AddFiniFunction(DumpSavedValues, 0);

	PIN_StartProgram();
	return 0;
}

