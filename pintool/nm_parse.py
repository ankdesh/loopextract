#!/usr/bin/python

import sys
import subprocess

def main():
	file = sys.argv[1]
	cmd = "nm %s | grep " % sys.argv[2]
	fd = open(file)
	for line in fd:
		lst1 = line.split(",")
		command = cmd + lst1[0]
		output = subprocess.check_output(command, shell=True, universal_newlines=True)
		if output.find("\n") != -1:
			for l in output.split("\n"):
				lst2 = l.split(" ")
				if len(lst2) >= 2 and lst2[2] == lst1[0]:
					print "%s,%s,%s,%s" % (lst1[0], lst2[0], lst1[1], lst1[2].strip())
		else:
			lst2 = output.split(" ")
			if len(lst2) >= 2 and lst2[2] == lst1[0]:
				print "%s,%s,%s,%s" % (lst1[0], lst2[0], lst1[1], lst1[2].strip())
	fd.close()

if __name__ == "__main__":
	if len(sys.argv) < 3:
		print >> sys.stderr, "Usage %s <variable file> <executable>" % sys.argv[0]
	else:
		main()
